package com.riri.assestment.phonebookapp.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.riri.assestment.phonebookapp.dao.ContactDao;
import com.riri.assestment.phonebookapp.entity.ContactEntity;

@Database(entities = {ContactEntity.class}, version = 1)
public abstract class BaseDatabase extends RoomDatabase {
    public abstract ContactDao contactDao();
}
