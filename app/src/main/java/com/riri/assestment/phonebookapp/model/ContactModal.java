package com.riri.assestment.phonebookapp.model;

import androidx.databinding.BaseObservable;

public class ContactModal extends BaseObservable {

    private int id;
    private String name;
    private String phoneNumber;

    public ContactModal() {

    }
    public ContactModal(int id,String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
