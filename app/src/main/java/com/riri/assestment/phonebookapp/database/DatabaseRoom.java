package com.riri.assestment.phonebookapp.database;

import android.content.Context;

import androidx.room.Room;

public class DatabaseRoom {

    private Context mCtx;
    private static DatabaseRoom mInstance;

    //our app database object
    private BaseDatabase appDatabase;

    private DatabaseRoom(Context mCtx) {
        this.mCtx = mCtx;

        //creating the app database with Room database builder
        //MyToDos is the name of the database
        appDatabase = Room.databaseBuilder(mCtx, BaseDatabase.class, "MyContact").build();
    }

    public static synchronized DatabaseRoom getInstance(Context mCtx) {
        if (mInstance == null) {
            mInstance = new DatabaseRoom(mCtx);
        }
        return mInstance;
    }

    public BaseDatabase getAppDatabase() {
        return appDatabase;
    }

}
