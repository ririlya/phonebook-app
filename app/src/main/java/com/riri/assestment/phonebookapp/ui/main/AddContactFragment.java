package com.riri.assestment.phonebookapp.ui.main;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.riri.assestment.phonebookapp.R;
import com.riri.assestment.phonebookapp.databinding.FragmentAddContactBinding;
import com.riri.assestment.phonebookapp.model.ContactModal;

public class AddContactFragment extends Fragment {

    private static final String IS_NEW_CONTACT_PARAM = "IS_NEW_CONTACT_PARAM";
    private static final String CONTACT_DATA = "CONTACT_DATA";

    private FragmentAddContactBinding binding;
    private boolean isIsNewContact;
    private MainViewModel mViewModel;
    private ContactModal currentContactEdit = new ContactModal();


    public static AddContactFragment newInstance(boolean isitNewContact, String contactData) {
        AddContactFragment fragment = new AddContactFragment();
        Bundle args = new Bundle();
        args.putBoolean(IS_NEW_CONTACT_PARAM, isitNewContact);
        args.putString(CONTACT_DATA, contactData);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isIsNewContact = getArguments().getBoolean(IS_NEW_CONTACT_PARAM);
            String getArgumentContact = getArguments().getString(CONTACT_DATA);
            if(getArgumentContact != null){
                currentContactEdit = new Gson().fromJson(getArgumentContact, ContactModal.class);
//                Log.e("REPO","ID CONTACT 7 -> " + currentContactEdit.getId());

            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentAddContactBinding.inflate(inflater, container, false);

        mViewModel = new ViewModelProvider(this,new MainViewModel(getContext())).get(MainViewModel.class);

        binding.toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        binding.toolbar.setTitle(R.string.new_contact);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        if(!isIsNewContact){
            binding.etContactName.setText(currentContactEdit.getName());
            binding.etContactNumber.setText(currentContactEdit.getPhoneNumber());
            binding.toolbar.setTitle(R.string.edit_contact);
            binding.btnDelete.setVisibility(View.VISIBLE);
        }


        binding.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContactModal contactModal = new ContactModal();
                contactModal.setName(binding.etContactName.getText().toString().trim());
                contactModal.setPhoneNumber(binding.etContactNumber.getText().toString().trim());
                if(!isIsNewContact){
                    contactModal.setId(currentContactEdit.getId());
                }
                mViewModel.saveContact(isIsNewContact,contactModal);
                getActivity().onBackPressed();
            }
        });

        binding.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContactModal contactModal = new ContactModal();
                contactModal.setName(binding.etContactName.getText().toString().trim());
                contactModal.setPhoneNumber(binding.etContactNumber.getText().toString().trim());
                contactModal.setId(currentContactEdit.getId());
                mViewModel.deleteContact(contactModal);
                getActivity().onBackPressed();
            }
        });


        return binding.getRoot();
    }
}