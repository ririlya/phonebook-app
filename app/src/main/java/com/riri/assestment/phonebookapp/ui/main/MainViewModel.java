package com.riri.assestment.phonebookapp.ui.main;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.databinding.ObservableArrayList;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.riri.assestment.phonebookapp.entity.ContactEntity;
import com.riri.assestment.phonebookapp.model.ContactModal;
import com.riri.assestment.phonebookapp.repository.ContactRepository;

import java.util.ArrayList;
import java.util.List;

public class MainViewModel extends ViewModel implements ViewModelProvider.Factory{
    private ContactRepository repository;
    public MutableLiveData<List<ContactModal>> contactListMutable = new MutableLiveData<>();
    private Context mContext;

    public MainViewModel(Context context) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                repository = new ContactRepository(context);
//                getContactList();
            }
        });
        mContext = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new MainViewModel(mContext);
    }

    public void getContactList(){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                List<ContactEntity> listContactEntity = repository.getContactFromDatabase();
                List<ContactModal> listContactModal = new ArrayList<>();
                for(int i = 0; i < listContactEntity.size(); i++){
//                    Log.e("REPO","ID CONTACT 5 -> " + listContactEntity.get(i).getId());

                    listContactModal.add(
                            new ContactModal(
                                    listContactEntity.get(i).getId(),
                                    listContactEntity.get(i).getContactName(),
                                    listContactEntity.get(i).getContactPhoneNum()
                            )
                    );
                }
                contactListMutable.postValue(listContactModal);
            }
        });
    }

    public void saveContact(boolean isItNew,ContactModal currentContact){
        ContactEntity newContactEntity = new ContactEntity();
        newContactEntity.setContactName(currentContact.getName());
        newContactEntity.setContactPhoneNum(currentContact.getPhoneNumber());
        /**
         *  Insert and get data using Database Async way
         */
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                if(isItNew){
                    repository.addContact(newContactEntity);
                }
                else{
                    newContactEntity.setId(currentContact.getId());
//                    Log.e("REPO","ID CONTACT 8 -> " + newContactEntity.getId());
                    repository.updateContact(newContactEntity);
                    getContactList();
                }
            }
        });
    }

    public void deleteContact(ContactModal currentContact){
        ContactEntity newContactEntity = new ContactEntity();
        newContactEntity.setContactName(currentContact.getName());
        newContactEntity.setContactPhoneNum(currentContact.getPhoneNumber());
        newContactEntity.setId(currentContact.getId());
        /**
         *  Insert and get data using Database Async way
         */
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                repository.deleteContact(newContactEntity);
            }
        });
    }


}