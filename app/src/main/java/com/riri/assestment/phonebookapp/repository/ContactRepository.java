package com.riri.assestment.phonebookapp.repository;

import android.content.Context;
import android.util.Log;

import com.riri.assestment.phonebookapp.dao.ContactDao;
import com.riri.assestment.phonebookapp.database.DatabaseRoom;
import com.riri.assestment.phonebookapp.entity.ContactEntity;

import java.util.ArrayList;
import java.util.List;

public class ContactRepository {
    private Context context;
    private ContactDao contactDao;

    public ContactRepository(Context context) {
        this.context = context;
        this.contactDao = DatabaseRoom.getInstance(context).getAppDatabase().contactDao();
    }

    public List<ContactEntity> getContactFromDatabase(){
        List<ContactEntity> listContactEntity = contactDao.getAll();
        return listContactEntity;
    }

    public boolean addContact(ContactEntity contactEntity){
        contactDao.insert(contactEntity);
        return true;
    }

    public boolean updateContact(ContactEntity contactEntity){
        Log.e("REPO","ID CONTACT 2 -> " + contactEntity.getId());
        int check = contactDao.updateTest(contactEntity.getId(),contactEntity.getContactName(),contactEntity.getContactPhoneNum());
        Log.e("REPO", "STATUS UPDATE " +check);
        return true;
    }

    public boolean deleteContact(ContactEntity contactEntity){
//        contactDao.delete(contactEntity);
//        Log.e("REPO","ID CONTACT 3 -> " + contactEntity.getId());
        contactDao.deleteContactById(contactEntity.getId());
        return true;
    }

}
