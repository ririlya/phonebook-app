package com.riri.assestment.phonebookapp.ui.main;

import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.riri.assestment.phonebookapp.R;
import com.riri.assestment.phonebookapp.adapter.ContactAdapter;
import com.riri.assestment.phonebookapp.databinding.MainFragmentBinding;
import com.riri.assestment.phonebookapp.model.ContactModal;

import java.util.ArrayList;
import java.util.List;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;
    private MainFragmentBinding binding;
    private ContactAdapter adapter;
    private String currentText = "";
    List<ContactModal> currentListContact = new ArrayList<>();

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = MainFragmentBinding.inflate(inflater, container, false);

        mViewModel = new ViewModelProvider(this,new MainViewModel(getContext())).get(MainViewModel.class);
        initRecyclerView();
        initSearchBar();

        binding.fbAddContact.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                // Create new fragment and transaction
                Fragment newFragment = AddContactFragment.newInstance(true,null);
                FragmentTransaction transaction = getParentFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack
                transaction.replace(R.id.container, newFragment);
                transaction.addToBackStack(null);

                // Commit the transaction
                transaction.commit();
            }
        });

        return binding.getRoot();
    }


    private void initRecyclerView() {
        binding.rvContact.setLayoutManager(new LinearLayoutManager(binding.rvContact.getContext()));
        adapter = new ContactAdapter(binding.rvContact.getContext(), currentListContact,( obj, position) -> {

            Log.e("REPO","ID CONTACT -> " + obj.getId());

            Fragment newFragment = AddContactFragment.newInstance(false,new Gson().toJson(obj));
            FragmentTransaction transaction = getParentFragmentManager().beginTransaction();

            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack
            transaction.replace(R.id.container, newFragment);
            transaction.addToBackStack(null);

            // Commit the transaction
            transaction.commit();
        });
        binding.rvContact.setAdapter(adapter);
        mViewModel.getContactList();

        mViewModel.contactListMutable.observe(getViewLifecycleOwner(), new Observer<List<ContactModal>>() {
                    @Override
                    public void onChanged(List<ContactModal> contactModals) {
                        currentListContact = contactModals;
                        adapter.setData(contactModals);
                        adapter.notifyDataSetChanged();
                    }
                }
        );
    }

    private void initSearchBar(){
        binding.ivClearText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.etSearchContact.setText("");
                currentText = "";
                adapter.setData(currentListContact);
                adapter.notifyDataSetChanged();
            }
        });

        binding.etSearchContact.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch();
                    return true;
                }
                return false;
            }
        });

        binding.etSearchContact.addTextChangedListener( new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.toString().trim().length()==0){
                    binding.ivClearText.setVisibility(View.GONE);

                    adapter.setData(currentListContact);
                    adapter.notifyDataSetChanged();
                } else {
                    binding.ivClearText.setVisibility(View.VISIBLE);
                    currentText = s.toString().trim();
                    performSearch();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void afterTextChanged(Editable s) { }
        });
    }

    private void performSearch(){
        adapter.getFilter().filter(currentText);
    }

}