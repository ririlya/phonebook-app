package com.riri.assestment.phonebookapp.adapter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.riri.assestment.phonebookapp.R;
import com.riri.assestment.phonebookapp.databinding.ItemContactRowBinding;
import com.riri.assestment.phonebookapp.model.ContactModal;

import java.util.ArrayList;
import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder>{

    private List<ContactModal> contacts;
    private List<ContactModal> filtered_icontacts = new ArrayList<>();
    private ItemFilter mFilter = new ItemFilter();
    private Context context;
    private OnItemClickListener mOnItemClickListener;
    private AsyncListDiffer<ContactModal> mAsyncListDiffer;

    public interface OnItemClickListener {
        void onItemClick(ContactModal currentContact, int position);
    }

    public ContactAdapter(Context context, List<ContactModal> contacts,final OnItemClickListener mItemClickListener) {
        this.context = context;
        this.contacts = contacts;
        this.filtered_icontacts = contacts;
        this.mOnItemClickListener = mItemClickListener;

        DiffUtil.ItemCallback<ContactModal> diffUtilCallback = new DiffUtil.ItemCallback<ContactModal>() {
            @Override
            public boolean areItemsTheSame(@NonNull ContactModal newUser, @NonNull ContactModal oldUser) {
                return newUser.getName().equals(oldUser.getName());
            }

            @Override
            public boolean areContentsTheSame(@NonNull ContactModal newUser, @NonNull ContactModal oldUser) {
                return newUser.getName().equals(oldUser.getName());
            }
        };
        mAsyncListDiffer = new AsyncListDiffer<>(this, diffUtilCallback);
    }

    class ContactViewHolder extends RecyclerView.ViewHolder {

        private final ItemContactRowBinding binding;

        ContactViewHolder(ItemContactRowBinding itemContactBinding) {
            super(itemContactBinding.getRoot());
            this.binding = itemContactBinding;
        }
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        ItemContactRowBinding view = ItemContactRowBinding.inflate(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {
        ContactModal currentContact = mAsyncListDiffer.getCurrentList().get(position);

        holder.binding.contactName.setText(currentContact.getName());
        holder.binding.contactNumber.setText(currentContact.getPhoneNumber());
//        Log.e("REPO","ID CONTACT 6 -> " + currentContact.getId());

        holder.binding.llContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemClickListener.onItemClick(currentContact,holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return  mAsyncListDiffer.getCurrentList().size();
    }

    public void setData(List<ContactModal> data){
        mAsyncListDiffer.submitList(data);

    }

    /**
     * Gets filter.
     *
     * @return the filter
     */
    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String query = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();
            final List<ContactModal> list = mAsyncListDiffer.getCurrentList();
            final List<ContactModal> result_list = new ArrayList<>(list.size());
            for (int i = 0; i < list.size(); i++) {
                String str_title = list.get(i).getName();
                if (str_title.toLowerCase().contains(query)) {
                    result_list.add(list.get(i));
                }
            }
            results.values = result_list;
            results.count = result_list.size();
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filtered_icontacts = (List<ContactModal>) results.values;
            mAsyncListDiffer.submitList(filtered_icontacts);
        }
    }
}
