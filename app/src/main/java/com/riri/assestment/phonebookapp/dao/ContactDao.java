package com.riri.assestment.phonebookapp.dao;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.riri.assestment.phonebookapp.entity.ContactEntity;

import java.util.List;

@Dao
public interface ContactDao {

    @Query("SELECT * FROM contact")
    List<ContactEntity> getAll();

    @Insert
    void insert(ContactEntity contact);

    @Delete
    void delete(ContactEntity contact);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int update(ContactEntity contact);

    @Query("UPDATE contact SET contactName=:contactName, contactPhoneNum=:phoneNum WHERE id = :contact_id")
    int updateTest( int contact_id,String contactName,String phoneNum);

    @Query("DELETE FROM contact WHERE id = :contact_id")
    void deleteContactById(int contact_id);

}
